# File:		allocate.asm
# Author:	Shane Hale (smh1931@cs.rit.edu)
# Description:	Memory allocation.
#

# Constants
WALL =	35

	.data

#
# Memory allocation for up to 80x80 board
# 6400 characters * 8 bits for each character
maze_mem:
	.space 51200
scratch_mem:
	.space 51200

	.text	

	.globl	allocate

# Name:		allocate
# Description:	allocates memory for an 80x80 maze
#		51200 bytes = 8 bytes * 80 columns * 80 rows
# Arguments:	none
# Returns:	v0: address of the allocated memory
#		v1: address of scratch maze for solving
allocate:
	la	$t0, maze_mem
	la	$t3, scratch_mem
	li	$t1, 6400
	li	$t2, WALL

# Name:		loop_allocate
# Description:	allocate the memory for 80 columns and 80 rows
# Arguments:	none
# Returns:	nothing
loop_allocate:
	beqz	$t1, done_allocate
	sw	$t2, 0($t0)
	sw	$t2, 0($t3)
	addi	$t0, $t0, 8
	addi	$t3, $t3, 8
	addi	$t1, $t1, -1
	j	loop_allocate

# Name:		done_allocate
# Description:	sets the return variables and returns
# Arguments:	none
# Returns:	v0: address of the allocated memory
#		v1: address of scratch maze for solving
done_allocate:
	la	$v0, maze_mem
	la	$v1, scratch_mem
	
	jr	$ra
