# File:		print.asm
# Author:	Shane Hale (smh1931@cs.rit.edu)
# Description:	File to handle print functions
#		for the maze solver

# Constants
PRINT_INT =	1
PRINT_STRING =	4
PRINT_CHAR =	11
NEWLINE_CHAR =	10

	.data
	.align 2

# Banner for the program
banner:
	.asciiz "===\n=== Maze Solver\n=== by\n=== Shane Hale\n===\n\n"

# Banner for the maze input
input:
	.asciiz "\nInput Maze:"

# Banner for the maze solution
solution:
	.asciiz "\n\nSolution:\n"

	.text

	.globl	print_banner
	.globl	print_input
	.globl	print_solution
	.globl	print_maze

# Name:		print_banner
# Description:	Prints the banner at the start of the program
# Arguments:	none
# Returns:	nothing
print_banner:
	la	$a0, banner
	li	$v0, PRINT_STRING
	syscall
	jr	$ra

# Name:		print_input
# Description:	Prints the 'Input Maze' line
# Arguments:	nothing
# Returns:	nothing
print_input:
	la	$a0, input
	li	$v0, PRINT_STRING
	syscall
	jr	$ra

# Name:		print_solution
# Description:	Prints the 'Solution' line
# Arguments:	none
# Returns:	nothing
print_solution:
	la	$a0, solution
	li	$v0, PRINT_STRING
	syscall
	jr	$ra

# Name:		print_maze
# Description:	Prints the current maze
# Arguments:	a0: location of the maze
#		a1: height of the maze
#		a2: width of the maze
# Returns:	nothing
print_maze:

	addi	$sp, $sp, -16		# Save to stack
	sw	$s0, 0($sp)
	sw	$s1, 4($sp)
	sw	$s2, 8($sp)
	sw	$ra, 12($sp)

	move	$t0, $a0		# Maze memory location
	move	$t1, $a1		# Maze height
	move	$t2, $a2		# Maze width
	move	$s0, $zero		# current column
	move	$s1, $zero		# current row	

# Name:		loop_rows
# Description:	prints a new line for the maze
# Arguments:	nothing
# Returns:	nothing
loop_rows:
	li	$a0, NEWLINE_CHAR
	li	$v0, PRINT_CHAR
	syscall	

	beq	$s1, $t1, done_loop
	li	$s0, 0			# reset current column to 0
	addi	$s1, $s1, 1		# add 1 to current row

# Name:		loop_cols
# Description:	prints the contents of the maze
# Arguments:	none
# Returns:	nothing
loop_cols:
	beq	$s0, $t2, loop_rows

	lw	$a0, 0($t0)
	li	$v0, PRINT_CHAR
	syscall

	addi	$t0, $t0, 8		# move to next item in maze
	addi	$s0, $s0, 1		# add 1 to current column

	j	loop_cols

# Name:		done_loop
# Description:	returns to the calling function
# Arguments:	none
# Returns:	nothing
done_loop:
	lw	$s0, 0($sp)
	lw	$s1, 4($sp)
	lw	$s2, 8($sp)
	lw	$ra, 12($sp)
	addi	$sp, $sp, 16

	jr	$ra
