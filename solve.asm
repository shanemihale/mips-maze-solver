# File:		solve.asm
# Author:	Shane Hale (smh1931@cs.rit.edu)
# Description:	File to solve a maze

# Constants
START_SYMBOL =	83
SPACE_SYMBOL =	32
END_SYMBOL =	69
NL_SYMBOL =	10
CRUMB_SYMBOL =	46

	.data
	.align 2

	.text
	.globl	print_maze
	.globl	locate_start
	.globl	solve

# Name:		solve
# Description:	Attempts to solve the maze
# Arguments:	a0: location of the maze
#		a1: location of scratch maze
#		a2: width of the maze
# Returns	v0: maze solution
solve:
	move	$s0, $a0		# Location of maze
	move	$s1, $a1		# Location of scratch maze
	move	$s2, $a2		# Width of maze
	li	$s3, 0			# Current direction (north)

	addi	$sp, $sp, -12
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)

	jal	locate_start		# locate the S character

	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	addi	$sp, $sp, 12

	move	$t2, $v0		# Start character index

	li	$t3, 8			# Locate start in array
	mult	$t2, $t3
	mflo	$t3

	add	$s0, $s0, $t3		# Start in original maze
	add	$s1, $s1, $t3		# Start in scratch maze

	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

# Name:		step2
# Description:	Check the contents of the maze.
#		If location contains an 'E', goto step 5
#		If location contains 'S', or ' ' goto step 3
# Arguments:	none
# Returns:	nothing
step2:
	lw	$t3, 0($s0)
	lw	$t4, 0($s1)
	
	li	$t5, START_SYMBOL
	beq	$t3, $t5, step3		# Found S
	li	$t5, SPACE_SYMBOL
	beq	$t3, $t5, step3		# Found open space
	li	$t5, END_SYMBOL
	beq	$t3, $t5, step5		# Found E

# Name:		step3
# Description:	Look at the next character in the direction that
#		it is currently facing, in the maze and scratch
#		maze. This will leave a breadcrumb, or turn
#		counterclockwise to view the next item. If no new
#		route is found, a backtrack will occur.
# Arguments:	none
# Returns:	nothing
step3:
	li	$t7, 4
	bge	$t8, $t7, backtrack		# More than 4 rotations
	j	no_backtrack

# Name:		backtrack
# Description:	Perform a backtrack in the maze
# Arguments:	none
# Returns:	nothing
backtrack:	
	move	$a0, $s0		# Location of maze
	move	$a1, $s1		# Location of scratch maze
	move	$a2, $s2
	jal	move_backtrack
	move	$s0, $v0		# Replace maze with backtrack
	move	$s1, $v1		# Replace scratch with backtrack
	li	$s3, 0			# Reset direction to north
	li	$t8, 0			# Reset number of rotations

# Name:		no_backtrack
# Description:	Occurs when no backtrack is needed
# Arguments:	none
# Returns:	nothing
no_backtrack:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	move	$a0, $s0
	move	$a1, $s3
	move	$a2, $s2
	jal	peek_location			# peek original maze

	li	$t5, SPACE_SYMBOL		# If space open
	seq	$s4, $v0, $t5

	move	$a0, $s1
	move	$a1, $s3
	move	$a2, $s2
	jal	peek_location			# peek scratch maze

	li	$t5, SPACE_SYMBOL		# If space open
	seq	$s5, $v0, $t5

	and	$t5, $s4, $s5			# Space is open in both
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	li	$t4, 1
	beq	$t5, $t4, step4			# Found open space

	li	$t5, END_SYMBOL			# Found end symbol
	seq	$s5, $v0, $t5	
	beq	$s5, $t4, step5

	addi	$s3, $s3, 1			# Rotate direction
	addi	$t8, $t8, 1

	li	$t7, 4
	div	$s3, $t7
	mfhi	$s3

	j	step2

# Name:		step4
# Description:	Move into the empty space by adjusting location in the
#		maze, and the scratch maze. Allows the trail to be followed
#		back from the E to the S.
# Arguments:	none
# Returns:	nothing
step4:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	move	$a0, $s0			# Location of maze
	move	$a1, $s3			# Direction facing
	move	$a2, $s2			# Width of maze
	jal	move_location			# Move to new location

	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	move	$s0, $v0			# Copy maze

	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	move	$a0, $s1			# Location of scratch maze
	move	$a1, $s3			# Direction facing
	move	$a2, $s2			# Width of maze
	jal	move_location			# Move to new location

	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	move	$s1, $v0			# Copy maze

	addi	$t5, $s3, 48
	sw	$t5, 0($v0)			# Drop a breadcrumb

	li	$t8, 0

	j	step2

# Name:		step5
# Description:	Found the exit, follow the moves back to the start
# Arguments:	none
# Returns:	nothing
step5:
	move	$a0, $s0			# Location of maze
	move	$a1, $s1			# Location of scratch
	move	$a2, $s2			# Direction facing
	jal	follow_path			# Follow the path

	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	jr	$ra

# Name:		follow_path
# Description:	Follows the E back to the S, and lays dots
# Arguments:	a0: location of original maze
# 		a1: location of scratch maze
#		a2: width of maze
# Returns:	nothing
follow_path:

	lw	$t4, 0($a1)
	li	$t1, START_SYMBOL
	beq	$t1, $t4, done_following

	li	$t2, CRUMB_SYMBOL
	sw	$t2, 0($a0)

	li	$t0, 0
	li	$t1, 1
	li	$t2, 2
	li	$t3, 3
	li	$t4, 8

	beq	$s3, $t0, follow_south
	beq	$s3, $t1, follow_west
	beq	$s3, $t2, follow_north
	beq	$s3, $t3, follow_east

	j	done_moving

# Name:		follow_north
# Description:	Follow the path to the north
# Arguments:	none
# Returns:	nothing
follow_north:
	mult	$t4, $a2
	mflo	$t6
	sub	$a0, $a0, $t6
	sub	$a1, $a1, $t6
	j	done_moving

# Name:		follow_south
# Description:	Follow the path to the south
# Arguments:	none
# Returns:	nothing
follow_south:
	mult	$t4, $a2
	mflo	$t6
	add	$a0, $a0, $t6
	add	$a1, $a1, $t6
	j	done_moving

# Name:		follow_east
# Description:	Follow the path to the east
# Arguments:	none
# Returns:	nothing
follow_east:
	addi	$a0, $a0, 8
	addi	$a1, $a1, 8
	j	done_moving

# Name:		follow_west
# Desctiption:	Follow the path to the west
# Arguments:	none
# Returns:	nothing
follow_west:
	addi	$a0, $a0, -8
	addi	$a1, $a1, -8
	j	done_moving

# Name:		done_moving
# Description:	Determines the current value at the location and
#		calls the follow_path function, to continue to move
#		towards the 'S' symbol.
# Arguments:	none
# Returns:	nothing
done_moving:
	li	$t0, 0x30
	lw	$s3, 0($a1)
	sub	$s3, $s3, $t0	
	j	follow_path

# Name:		done_following
# Description:	Returns the function
# Arguments:	none
# Returns:	nothing
done_following:
	jr	$ra	

# Name:		move_backtrack
# Description:	Backtracks the the previous position
#		0 = North 1 = East 2 = South 3 = West
# Arguments:	a0: memory location of maze
#		a1: memory location of scratch maze
#		a2: width of maze
# Returns:	nothing
move_backtrack:
	li	$t0, 0x30		# North
	li	$t1, 0x31		# East
	li	$t2, 0x32		# South
	li	$t3, 0x33		# West
	move	$t8, $a0		# Original maze
	move	$t9, $a1		# Scratch maze

	lw	$t6, 0($t9)		# Determine direction

	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	beq	$t6, $t2, backtrack_north
	beq	$t6, $t3, backtrack_east
	beq	$t6, $t0, backtrack_south
	beq	$t6, $t1, backtrack_west

	j	backtrack_return

# Name:		backtrack_north
# Description:	Performs a backtrack north
# Arguments:	none
# Returns:	nothing
backtrack_north:
	move	$a0, $t8
	li	$a1, 0			# Backtrack original maze
	jal	move_location
	move	$t8, $v0
	
	move	$a0, $t9		# Backtrack scratch maze
	li	$a1, 0
	jal	move_location
	move	$t9, $v0

	j	backtrack_return

# Name:		backtrack_east
# Description:	Performs a backtrack east
# Arguments:	none
# Returns:	nothing	
backtrack_east:
	move	$a0, $t8
	li	$a1, 1			# Backtrack original maze
	jal	move_location
	move	$t8, $v0
	
	move	$a0, $t9		# Backtrack scratch maze
	li	$a1, 1
	jal	move_location
	move	$t9, $v0

	j	backtrack_return

# Name:		backtrack_south
# Description:	Performs a backtrack south
# Arguments:	none
# Returns:	nothing
backtrack_south:
	move	$a0, $t8
	li	$a1, 2			# Backtrack original maze
	jal	move_location
	move	$t8, $v0
	
	move	$a0, $t9		# Backtrack scratch maze
	li	$a1, 2
	jal	move_location
	move	$t9, $v0

	j	backtrack_return

# Name:		backtrack_west
# Description:	Performs a backtrack west
# Arguments:	none
# Returns:	nothing
backtrack_west:
	move	$a0, $t8
	li	$a1, 3			# Backtrack original maze
	jal	move_location
	move	$t8, $v0
	
	move	$a0, $t9		# Backtrack scratch maze
	li	$a1, 3
	jal	move_location
	move	$t9, $v0

	j	backtrack_return

# Name:		backtrack_return
# Description:	Returns the function after a backtrack
# Arguments:	none
# Returns:	v0: the new maze
#		v1: the new scratch maze
backtrack_return:
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	move	$v0, $t8
	move	$v1, $t9

	jr	$ra

# Name:		locate_start
# Description:	Locates the start symbol
# Arguments:	a0: location of the maze
# Returns:	v0: array index of the start symbol
locate_start:
	
	move	$s0, $a0		# Address of maze memory
	
	li	$t0, START_SYMBOL	# S character
	li	$t1, 0			# current index

# Name:		loop_find_start
# Description:	Loop and try to locate the 'S' symbol
# Arguments:	none
# Returns:	nothing
loop_find_start:
	lw	$t2, 0($s0)

	beq	$t0, $t2, found_start

	addi	$t1, $t1, 1
	addi	$s0, $s0, 8
	j	loop_find_start

# Name:		found_start
# Description:	Returns the index of 'S' and returns
# Arguments:	none
# Returns:	v0: the index of the 'S' symbol
found_start:
	move	$v0, $t1		# Location of S
	jr	$ra

# Name:		peek_location
# Description:	Returns the next character in the current direction
#		0 = North 1 = East 2 = South 3 = West
# Arguments:	a0: memory location of maze
#		a1: direction currently facing
#		a2: width of maze
# Returns:	the next element that currently facing
peek_location:
	li	$t0, 0			# North
	li	$t1, 1			# East
	li	$t2, 2			# South
	li	$t3, 3			# West
	li	$t4, 8
	move	$t5, $a0

	beq	$a1, $t0, peek_north
	beq	$a1, $t1, peek_east
	beq	$a1, $t2, peek_south
	beq	$a1, $t3, peek_west

	j	return_peek

# Name:		peek_north
# Description:	Performs a peek to the north
# Arguments:	none
# Returns:	v0: the value in the north direction
peek_north:
	mult	$t4, $a2
	mflo	$t6
	sub	$t5, $t5, $t6

	lw	$v0, 0($t5)
	j	return_peek

# Name:		peek_east
# Description:	Performs a peek to the east
# Arguments:	none
# Returns:	v0: the value in the east direction
peek_east:
	addi	$t5, $t5, 8

	lw	$v0, 0($t5)
	j	return_peek

# Name:		peek_south
# Description:	Performs a peek to the south
# Arguments:	none
# Returns:	v0: the value in the south direction
peek_south:
	mult	$t4, $a2
	mflo	$t6
	add	$t5, $t5, $t6

	lw	$v0, 0($t5)
	j	return_peek

# Name:		peek_west
# Description:	Performs a peek to the west
# Arguments:	none
# Returns:	v0: the value in the west direction
peek_west:
	addi	$t5, $t5, -8

	lw	$v0, 0($t5)
	j	return_peek

# Name:		return_peek
# Description:	Returns the function
# Arguments:	none
# Returns:	nothing
return_peek:
	jr	$ra

# Name:		move_location
# Description:	Moves to the next character in the current direction
#		0 = North 1 = East 2 = South 3 = West
# Arguments:	a0: memory location of maze
#		a1: direction currently facing
#		a2: width of maze
# Returns:	nothing
move_location:
	li	$t0, 0			# North
	li	$t1, 1			# East
	li	$t2, 2			# South
	li	$t3, 3			# West
	li	$t4, 8
	move	$t5, $a0

	beq	$a1, $t0, move_north
	beq	$a1, $t1, move_east
	beq	$a1, $t2, move_south
	beq	$a1, $t3, move_west

	j	return_move

# Name:		move_north
# Description:	Performs a move to the north
# Arguments:	none
# Returns:	nothing
move_north:
	mult	$t4, $a2
	mflo	$t6
	sub	$t5, $t5, $t6

	lw	$v0, 0($t5)
	j	return_move

# Name:		move_east
# Description:	Performs a move to the east
# Arguments:	none
# Returns:	nothing
move_east:
	addi	$t5, $t5, 8

	lw	$v0, 0($t5)
	j	return_move

# Name:		move_south
# Description:	Performs a move to the south
# Arguments:	none
# Returns:	nothing
move_south:
	mult	$t4, $a2
	mflo	$t6
	add	$t5, $t5, $t6

	lw	$v0, 0($t5)
	j	return_move

# Name:		move_west
# Description:	Performs a move to the west
# Arguments:	none
# Returns:	nothing
move_west:
	addi	$t5, $t5, -8

# Name:		return_move
# Description:	Returns the maze after the move
# Arguments:	none
# Returns:	v0: the maze after the move
return_move:
	move	$v0, $t5
	jr	$ra
