# File:		maze.asm
# Author:	Shane Hale (smh1931@cs.rit.edu)
# Description:	Main file to run maze program.

# Constants
PRINT_INT =	1
PRINT_STRING =	4
READ_INT = 	5
READ_STRING =	8
EXIT = 		10

	.data
	.align 2

# Used to hold input
maze_input:
	.space	8
width:
	.word	0
height:
	.word	0

	.align 2

	.text
	
	.globl	main
	.globl	allocate
	.globl	print_banner
	.globl	print_input
	.globl	print_solution
	.globl	print_maze
	.globl	solve

# Name:		main
# Description:	The main function for the maze solver
# Arguments:	none
# Returns:	nothing
main:
	la	$a0, height
	la	$a1, width
	jal	read_dimensions		# Get dimensions of maze

	lw	$s1, 0($t0)		# s1 = height
	lw	$s2, 0($t1)		# s2 = width

	jal	print_banner

	jal	print_input

	jal	allocate

	move	$s0, $v0		# s0 = Pointer to maze memory
	move	$s3, $v1		# s3 = Pointer to scratch

	move	$a0, $v0
	move	$a1, $s1
	move	$a2, $s2
	jal	read_maze		# read data into maze

	move	$a0, $s0
	move	$a1, $s3
	jal	copy_maze		# copy maze to scratch

	move	$a0, $s0
	move	$a1, $s1
	move	$a2, $s2
	jal	print_maze		# print original maze

	addi	$sp, $sp, -32
	sw	$s0, 0($sp)
	sw	$s1, 4($sp)
	sw	$s2, 8($sp)
	sw	$s3, 12($sp)
	sw	$s4, 16($sp)
	sw	$s5, 20($sp)
	sw	$s6, 24($sp)
	sw	$s7, 28($sp)

	move	$a0, $s0
	move	$a1, $s3
	move	$a2, $s2
	jal	solve			# solve the maze

	lw	$s0, 0($sp)
	lw	$s1, 4($sp)
	lw	$s2, 8($sp)
	lw	$s3, 12($sp)
	lw	$s4, 16($sp)
	lw	$s5, 20($sp)
	lw	$s6, 24($sp)
	lw	$s7, 28($sp)
	addi	$sp, $sp, 32

	jal	print_solution		# print the solution header

	move	$a0, $s0
	move	$a1, $s1
	move	$a2, $s2
	jal	print_maze		# print the solution maze

	j exit				# exit the program

# Name:		exit
# Description:	Exits the program
# Arguments:	none
# Returns:	nothing
exit:
	li	$v0, EXIT
	syscall

# Name:		read_maze
# Description:	Read in a maze
# Arguments:	a0: Memory for maze
#		a1: height of the maze
#		a2: width of the maze
# Returms:	nothing
read_maze:

	move	$s0, $a0
	move	$s1, $a1
	move	$s2, $a2

	move	$t0, $a0		# Address of maze memory
	li	$t1, 0			# Current column
	li	$t2, 0			# Current row
	li	$t3, 10			# New line character

# Name:		loop_input_rows
# Description:	Loops through the rows of the input maze
# Arguments:	none
# Returns:	nothing
loop_input_rows:
	beq	$t2, $s1, done_input
	li	$t1, 0
	addi	$t2, $t2, 1

# Name:		loop_input_cols
# Description:	Loops through the columns of the input maze
# Arguments:	none
# Returns:	nothing
loop_input_cols:

	beq	$t1, $s2, loop_input_rows

	li	$v0, 12			# Input one character
	syscall

	beq	$v0, $t3, loop_input_cols

	sw	$v0, 0($t0)		# Store into array

	addi	$t0, $t0, 8		# Move to next character
	addi	$t1, $t1, 1

	j	loop_input_cols	

# Name:		done_input
# Description:	Prints a blank link and returns to the calling function
# Arguments:	none
# Returns:	nothing
done_input:
	li	$a0, 10
	li	$v0, 11
	syscall
	jr	$ra

# Name:		done_read
# Description:	Returns to the calling function
# Arguments:	none
# Returns:	nothing
done_read:
	jr	$ra

# Name:		read_dimensions
# Description:	Read the maze dimensions
# Arguments:	a0: Pointer to maze height
#		a1: Pointer to maze width
# Returns:	nothing
read_dimensions:
	move	$t0, $a0
	move	$t1, $a1
	
	addi	$sp, $sp, -4		# Save RA to stack
	sw	$ra, 0($sp)

	li	$v0, READ_INT		# Read the maze height
	syscall
	sw	$v0, 0($t0)
	
	li	$v0, READ_INT		# Read the maze width
	syscall
	sw	$v0, 0($t1)

	lw	$ra, 0($sp)		# Restore RA from stack
	addi	$sp, $sp, 4

	jr	$ra			# Return

# Name:		copy_maze
# Description:	Copy contents from one maze array to another
#		Assumes that the maze size is at most 80x80
# Arguments:	a0: pointer to the first maze
#		a1: pointer to the second maze
# Returns:	nothing
copy_maze:
	move	$t0, $a0
	move	$t1, $a1
	li	$t2, 6400		# Maximum size of maze
	li	$t3, 0

# Name:		copy_index
# Description:	Copies the contents from one maze row/column to
#		another maze
# Arguments:	none
# Returns:	nothing
copy_index:
	beq	$t2, $t3, done_copy

	lw	$t4, 0($t0)	
	sw	$t4, 0($t1)

	addi	$t0, $t0, 8
	addi	$t1, $t1, 8
	addi	$t3, $t3, 1
	j	copy_index

# Name:		done_copy
# Description:	Returns to the calling function when the maze
#		has been copied.
# Arguments:	none
# Returns:	nothing
done_copy:
	jr	$ra			# Return
